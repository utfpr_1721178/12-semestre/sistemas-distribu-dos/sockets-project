package distributed_system.sockets_project;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;

import distributed_system.sockets_project.controllers.MultiCast;
import distributed_system.sockets_project.controllers.Process;

public class Main {

	public static void main(String[] args) {

		try {
			new Process();
		} catch (NoSuchAlgorithmException | NoSuchProviderException | IOException | SignatureException
				| InvalidKeyException e
		) {
			e.printStackTrace();
		}

	}
}
