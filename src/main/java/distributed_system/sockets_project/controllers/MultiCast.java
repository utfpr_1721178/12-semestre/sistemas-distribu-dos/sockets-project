package distributed_system.sockets_project.controllers;

import distributed_system.sockets_project.model.Header;
import distributed_system.sockets_project.utils.ByteConversionTools;
import distributed_system.sockets_project.utils.GlobalInformation;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.util.Objects;

public final class MultiCast extends Thread {

	private final InetAddress inetAddress;

	private final MulticastSocket multicastSocket;

	private final Process process;

	public MultiCast(Process process) throws IOException {
		this.process = process;

		this.inetAddress = InetAddress.getByName(GlobalInformation.MULTICAST_ADDRESS);

		this.multicastSocket = new MulticastSocket(GlobalInformation.MULTICAST_PORT);

		this.multicastSocket.joinGroup(inetAddress);

	}

	public void stopListenMessages() {
		this.stop();
	}

	public void disconnect() throws IOException {
		this.stopListenMessages();
		multicastSocket.leaveGroup(inetAddress);
		multicastSocket.close();
	}

	public synchronized void sendMessage(final byte[] message) throws IOException {

		multicastSocket
				.send(new DatagramPacket(message, message.length, inetAddress, GlobalInformation.MULTICAST_PORT));

	}

	@Override
	public void run() {

		while (true) {

			final DatagramPacket messageReceive = new DatagramPacket(new byte[500_000], 500_000);

			try {
				multicastSocket.receive(messageReceive);

				Header header = (Header) ByteConversionTools.convertByteArrayToObject(messageReceive.getData());

				if (Objects.equals(header.getSenderProcessIdentification(), this.process.state.getCurrentProcessIdentifier())) {
					continue;
				}

				switch (header.getMessageType()) {
					case NEW_PROCESS_DETECTED:
						this.process.handlesReceivingANewProcessMessageInTheMulticastGroup(header);
						break;
					case NEWS:
						this.process.handlesReceivingAMessageContainingNewNews(header);
						break;
					case BYE_BYE:
						this.process.treatsReceivingTheMessageByeBye(header);
						break;
					case A_PROCESS_MADE_A_MISTAKE:
						this.process.handlesErrorWarningFromOtherProcesses(header);
						break;
					default:
						this.process.state.addNewLog("A message outside the known standards was received");
						break;
				}

			} catch (IOException | ClassNotFoundException | NoSuchProviderException | NoSuchAlgorithmException |
					InvalidKeyException | SignatureException e) {
				e.printStackTrace();
			}

		}
	}

}