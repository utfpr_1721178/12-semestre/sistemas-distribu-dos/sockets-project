package distributed_system.sockets_project.controllers;

import distributed_system.sockets_project.model.Header;
import distributed_system.sockets_project.utils.ByteConversionTools;

import java.io.IOException;
import java.net.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.util.Objects;

public class Unicast extends Thread {
	private final DatagramSocket unicastSocket;

	private final Process process;

	public Unicast(Process process) throws UnknownHostException, SocketException {
		this.process = process;

		// The operational system will choose a valid port to unicast socket
		this.unicastSocket = new DatagramSocket(0, InetAddress.getByName("localhost"));
	}

	// return unicast port
	public int getPort() {
		return this.unicastSocket.getLocalPort();
	}

	public InetAddress getLocalHostAddress() {
		return this.unicastSocket.getLocalAddress();
	}

	public synchronized void sendMessage(byte[] message, InetAddress address, int port) throws IOException {
		this.unicastSocket.send(new DatagramPacket(message, message.length, address, port));
	}

	public void disconnect(){
		this.stop();
	}

	@Override
	public void run() {

		while (true) {

			DatagramPacket messageReceived = new DatagramPacket(new byte[2048], 2048);

			Header message = null;

			try {
				this.unicastSocket.receive(messageReceived);

				message = (Header) ByteConversionTools.convertByteArrayToObject(messageReceived.getData());

				if (Objects.equals(message.getSenderProcessIdentification(), this.process.state.getCurrentProcessIdentifier())) {
					continue;
				}

				switch (message.getMessageType()) {
					case WELCOME_MESSAGE:
						this.process.handlesReceivingTheWelcomeMessage(message);
						break;
					default:
						this.process.state.addNewLog("A message outside the known standards was received");
						break;
				}

			} catch (IOException | ClassNotFoundException | InvalidKeyException | NoSuchAlgorithmException |
					NoSuchProviderException | SignatureException e) {
				e.printStackTrace();
			}

		}
	}
}