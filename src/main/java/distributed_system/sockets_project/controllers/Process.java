package distributed_system.sockets_project.controllers;

import distributed_system.sockets_project.model.Header;
import distributed_system.sockets_project.model.HeaderPrototype;
import distributed_system.sockets_project.model.ProcessIdentification;
import distributed_system.sockets_project.model.State;
import distributed_system.sockets_project.model.enums.MessagesTypes;
import distributed_system.sockets_project.model.enums.MistakeTypes;
import distributed_system.sockets_project.model.messages.*;
import distributed_system.sockets_project.utils.ByteConversionTools;
import distributed_system.sockets_project.utils.MessageSignaturesTools;
import distributed_system.sockets_project.utils.OutputTools;
import distributed_system.sockets_project.utils.ToolsForVerifyingNewsVeracity;
import distributed_system.sockets_project.view.TerminalOutput;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.util.Date;
import java.util.Objects;

public class Process {

	public State state;

	public MultiCast multiCast;

	public Unicast unicast;

	public TerminalOutput terminalOutput;

	public Process() throws IOException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException,
			InvalidKeyException {

		// Here the process should enter to the network group

		this.multiCast = new MultiCast(this);

		this.unicast = new Unicast(this);

		// Create own state
		this.state = new State(
				this,
				// Generate unique identification
				(int) ((new Date()).getTime()),
				MessageSignaturesTools.generateKeyPair(), this.unicast.getPort()
		);

		// Here are created the header prototype

		HeaderPrototype.factory(unicast.getPort(), this.state.getCurrentProcessIdentifier());

		this.terminalOutput = new TerminalOutput(this);

		this.unicast.start();
		this.multiCast.start();

		this.sendMessageToTheMulticastGroupAdvisingOfTheNewProcess();

		this.terminalOutput.userInput();
	}

	public void sendMessageToTheMulticastGroupAdvisingOfTheNewProcess() throws IOException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchProviderException, SignatureException {

		this.state.addNewLog("Sending acknowledgment message");

		byte[] messageObject = ByteConversionTools.convertObjectToByteArray(
				new NewProcess(this.state.getPublicKey())
		);

		this.multiCast.sendMessage(
				ByteConversionTools.convertObjectToByteArray(
						new Header(HeaderPrototype.getHeader(), MessagesTypes.NEW_PROCESS_DETECTED,
								messageObject,
								MessageSignaturesTools.sign(messageObject, this.state.getPrivateKey())
						)
				)
		);
	}

	public void handlesReceivingANewProcessMessageInTheMulticastGroup(Header header) throws IOException,
			ClassNotFoundException, NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {

		NewProcess bodyMessage = (NewProcess) ByteConversionTools.convertByteArrayToObject(header.getMessageBody());

		if (this.state.thisProcessExist(header.getSenderProcessIdentification())) {
			return;
		}

		if (!MessageSignaturesTools.validatesTheSignatureOnTheMessage(
				header.getMessageBody(),
				bodyMessage.getPublicKey(),
				header.getSignature()
		)) {
			this.state.addNewLog("A message with an invalid signature was received trying to go through the process " +
					"identified as " + header.getSenderProcessIdentification());
			return;
		}

		this.state.addNewLog("Acknowledgment message received from process identified as " +
				header.getSenderProcessIdentification());

		this.state.addIdentificationOfAnotherProcess(new ProcessIdentification(
				bodyMessage.getPublicKey(),
				header.getSenderProcessIdentification(),
				header.getUnicastSenderPort())
		);

		this.sendIdentificationMessage(header.getUnicastSenderPort());
	}

	public void sendIdentificationMessage(Integer recipientPort) throws IOException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchProviderException, SignatureException {

		byte[] messageObject = ByteConversionTools.convertObjectToByteArray(
				new Identification(this.state.getPublicKey())
		);

		this.unicast.sendMessage(
				ByteConversionTools.convertObjectToByteArray(
						new Header(HeaderPrototype.getHeader(), MessagesTypes.WELCOME_MESSAGE,
								messageObject,
								MessageSignaturesTools.sign(messageObject, this.state.getPrivateKey())
						)
				), this.unicast.getLocalHostAddress(), recipientPort
		);
	}

	public void handlesReceivingTheWelcomeMessage(Header header) throws IOException, ClassNotFoundException,
			InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException {

		Identification bodyMessage = (Identification) ByteConversionTools.convertByteArrayToObject(
				header.getMessageBody()
		);

		if (!MessageSignaturesTools.validatesTheSignatureOnTheMessage(
				header.getMessageBody(),
				bodyMessage.getPublicKey(),
				header.getSignature()
		)) {
			this.state.addNewLog("A message with an invalid signature was received trying to go through the process " +
					"identified as " + header.getSenderProcessIdentification());
			return;
		}

		this.state.addIdentificationOfAnotherProcess(new ProcessIdentification(
				bodyMessage.getPublicKey(),
				header.getSenderProcessIdentification(),
				header.getUnicastSenderPort())
		);
	}

	public void treatsReceivingTheMessageByeBye(Header header) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchProviderException, SignatureException {
		if (!this.state.thisProcessExist(header.getSenderProcessIdentification())) {
			this.state.addNewLog(
					"An unlisted process identified as " + header.getSenderProcessIdentification() +
							" has left the multicast group"
			);
			return;
		}

		if (!MessageSignaturesTools.validatesTheSignatureOnTheMessage(
				header.getMessageBody(),
				this.state.getProcessByIdentification(header.getSenderProcessIdentification()).getPublicKey(),
				header.getSignature())
		) {
			this.state.addNewLog(
					"A message with an invalid signature was received trying to kill the process identified as " +
							header.getSenderProcessIdentification()
			);
			return;
		}

		this.state.removeOneProcess(header.getSenderProcessIdentification());
		this.state.addNewLog(
				"The process identified as " + header.getSenderProcessIdentification() + " left the multicast group"
		);
	}

	private void sendsAMessageInformingTheOtherProcessesThatItIsLeavingTheMulticastGroup() throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchProviderException, SignatureException, IOException {

		byte[] messageBody = ByteConversionTools.convertObjectToByteArray(
				new ByeBye()
		);

		this.multiCast.sendMessage(
				ByteConversionTools.convertObjectToByteArray(
						new Header(
								HeaderPrototype.getHeader(),
								MessagesTypes.BYE_BYE,
								messageBody,
								MessageSignaturesTools.sign(messageBody, this.state.getPrivateKey())
						)
				)
		);
	}

	public void sendsAMessageContainingANewNews(boolean veracityOfTheNews) throws IOException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchProviderException, SignatureException {

		this.state.addNewLog("Sending " + (veracityOfTheNews ? "true" : "fake") + " news to the group");

		byte[] messageBody = ByteConversionTools.convertObjectToByteArray(
				new News(veracityOfTheNews)
		);

		this.multiCast.sendMessage(
				ByteConversionTools.convertObjectToByteArray(
						new Header(
								HeaderPrototype.getHeader(),
								MessagesTypes.NEWS,
								messageBody,
								MessageSignaturesTools.sign(messageBody, this.state.getPrivateKey())
						)
				)
		);
	}

	public void handlesReceivingAMessageContainingNewNews(Header header) throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchProviderException, SignatureException, IOException, ClassNotFoundException {
		if (!this.state.thisProcessExist(header.getSenderProcessIdentification())) {
			this.state.addNewLog(
					"An unlisted process identified as " + header.getSenderProcessIdentification() +
							" sent a report and was ignored"
			);
			return;
		}

		if (!MessageSignaturesTools.validatesTheSignatureOnTheMessage(
				header.getMessageBody(),
				this.state.getProcessByIdentification(header.getSenderProcessIdentification()).getPublicKey(),
				header.getSignature()
		)) {
			this.state.addNewLog("A message with an invalid signature was received trying to go through the process " +
					"identified as " + header.getSenderProcessIdentification());
			return;
		}

		News news = (News) ByteConversionTools.convertByteArrayToObject(header.getMessageBody());

		boolean verifiedNews = ToolsForVerifyingNewsVeracity.verifyTheTruthOfTheNews(
				news.getNews(),
				this.state.getProcessByIdentification(
						header.getSenderProcessIdentification()
				).getPercentageOfReliability(),
				this.state.getPercentageReliabilityOfCurrentProcess()
		);

		if (news.getNews() && verifiedNews) {
			this.state.increaseTheReliabilityOfTheCurrentProcess();

			this.state.increaseTheReliabilityOfAnotherProcess(header.getSenderProcessIdentification());

			this.state.addNewLog(
					"the process identified as " + header.getSenderProcessIdentification() +
							" sent true news and the current process was successfully analyzed, " +
							"both processes become more reliable"
			);

		} else if (news.getNews() && !verifiedNews) {
			this.state.decreaseTheReliabilityOfTheCurrentProcess();
			this.sendAMessageThatAGroupProcessHasMadeAnError(
					this.state.getCurrentProcessIdentifier(),
					MistakeTypes.FAILED_TO_CHECK_NEWS
			);

			this.state.increaseTheReliabilityOfAnotherProcess(header.getSenderProcessIdentification());

			this.state.addNewLog(
					"the process identified as " + header.getSenderProcessIdentification() + " sent true news, " +
							"but the current process erroneously classified it as false. " +
							"Process " + header.getSenderProcessIdentification() + " will have its reliability " +
							"increased and the current process will have its reliability decreased"
			);
		} else if (!news.getNews() && verifiedNews) {

			this.state.decreaseTheReliabilityOfTheCurrentProcess();
			this.sendAMessageThatAGroupProcessHasMadeAnError(
					this.state.getCurrentProcessIdentifier(),
					MistakeTypes.FAILED_TO_CHECK_NEWS
			);

			this.state.decreaseTheReliabilityOfAnotherProcess(header.getSenderProcessIdentification());
			this.sendAMessageThatAGroupProcessHasMadeAnError(
					header.getSenderProcessIdentification(),
					MistakeTypes.SEND_FAKE_NEWS
			);

			this.state.addNewLog(
					"the process identified as " + header.getSenderProcessIdentification() + " sent a false report, " +
							"but the current process erroneously classified as true. Both processes will have their " +
							"reliability decreased"
			);

		} else if (!news.getNews() && !verifiedNews) {

			this.state.increaseTheReliabilityOfTheCurrentProcess();

			this.state.decreaseTheReliabilityOfAnotherProcess(header.getSenderProcessIdentification());
			this.sendAMessageThatAGroupProcessHasMadeAnError(
					header.getSenderProcessIdentification(),
					MistakeTypes.SEND_FAKE_NEWS
			);

			this.state.addNewLog(
					"The process identified as " + header.getSenderProcessIdentification() + " sent a fake news and " +
							"the current process was successful in identifying it. Process " +
							header.getSenderProcessIdentification() + " will have reduced reliability and the current" +
							" process will have increased reliability."
			);
		}
	}

	public void sendAMessageThatAGroupProcessHasMadeAnError(Integer identification, MistakeTypes mistake) throws
			IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException {

		this.state.addNewLog("Warning other processes that the process identified as " + identification +
						" failed, " + (
						mistake == MistakeTypes.SEND_FAKE_NEWS ?
								"because it sent fake news" :
								"because it failed to verify news"
				)
		);

		byte[] messageBody = ByteConversionTools.convertObjectToByteArray(
				new Mistake(identification, mistake)
		);

		this.multiCast.sendMessage(
				ByteConversionTools.convertObjectToByteArray(
						new Header(
								HeaderPrototype.getHeader(),
								MessagesTypes.A_PROCESS_MADE_A_MISTAKE,
								messageBody,
								MessageSignaturesTools.sign(
										messageBody, this.state.getPrivateKey()
								)
						)
				)
		);
	}

	public void handlesErrorWarningFromOtherProcesses(Header header) throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchProviderException, SignatureException, IOException, ClassNotFoundException {

		if (!this.state.thisProcessExist(header.getSenderProcessIdentification())) {
			this.state.addNewLog(
					"An unlisted process, identified as " + header.getSenderProcessIdentification() +
							", sent an error message and was ignored"
			);
			return;
		}

		Mistake mistake = (Mistake) ByteConversionTools.convertByteArrayToObject(header.getMessageBody());

		if (!MessageSignaturesTools.validatesTheSignatureOnTheMessage(
				header.getMessageBody(),
				this.state.getProcessByIdentification(header.getSenderProcessIdentification()).getPublicKey(),
				header.getSignature())
		) {
			this.state.addNewLog(
					"A fake signature message was received from the process identified as " +
							header.getSenderProcessIdentification() + ", attempting to decrease the reliability of " +
							"the process identified as " + mistake.getIdentificationOfTheProcessThatMadeTheError()
			);
			return;
		}

		if (Objects.equals(mistake.getIdentificationOfTheProcessThatMadeTheError(), this.state.getCurrentProcessIdentifier())
				&& mistake.getWhatWasTheMistake() == MistakeTypes.SEND_FAKE_NEWS
		) {
			this.state.addNewLog("The process identified as " + header.getSenderProcessIdentification() +
					" discovered the fake news sent by the current process");
		} else {
			this.state.decreaseTheReliabilityOfAnotherProcess(mistake.getIdentificationOfTheProcessThatMadeTheError());

			this.state.addNewLog(mistake.getErrorMessage());
		}

	}

	public void sendMessageWithFakeSignature() throws IOException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchProviderException, SignatureException {

		this.state.addNewLog("Sending message with fake signature");

		byte[] messageBody = ByteConversionTools.convertObjectToByteArray(
				new News(false)
		);

		this.multiCast.sendMessage(
				ByteConversionTools.convertObjectToByteArray(
						new Header(
								HeaderPrototype.getHeader(),
								MessagesTypes.NEWS,
								messageBody,
								MessageSignaturesTools.sign(
										messageBody,
										MessageSignaturesTools.generateKeyPair().getPrivate()
								)
						)
				)
		);
	}

	public void closeTheApplication() throws IOException, NoSuchProviderException, NoSuchAlgorithmException,
			InvalidKeyException, SignatureException {

		this.sendsAMessageInformingTheOtherProcessesThatItIsLeavingTheMulticastGroup();

		this.multiCast.disconnect();

		this.unicast.disconnect();

		System.out.println("\n\n\n\n\n");
		OutputTools.drawLine();
		OutputTools.showCentralizedMessage("BYE BYE");
		OutputTools.drawLine();

		System.exit(0);
	}

}
