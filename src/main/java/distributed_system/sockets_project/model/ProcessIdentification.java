package distributed_system.sockets_project.model;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

import distributed_system.sockets_project.utils.GlobalInformation;

/**
 * The type Process identification.
 */
public class ProcessIdentification {

    private PublicKey publicKey;
    private PrivateKey privateKey;
    private Integer identification;
    private Integer unicastPort;
    private Integer percentageOfReliability = GlobalInformation.INITIAL_PERCENTAGE_OF_PROCESS_RELIABILITY;

    /**
     * Instantiates a new Process identification.
     */
    public ProcessIdentification(int unicastPort) {
        this.publicKey = null;
        this.privateKey = null;
        this.identification = null;
        this.unicastPort = unicastPort;
    }

    /**
     * Instantiates a new Process identification.
     *
     * @param keys the keys
     * @param id   the id
     */
    public ProcessIdentification(KeyPair keys, Integer id, Integer unicastPort) {
        this.setKeyPair(keys);
        this.setIdentification(id);
        this.setUnicastPort(unicastPort);
    }

    /**
     * Instantiates a new Process identification.
     *
     * @param publicKey the public key
     * @param id        the id
     */
    public ProcessIdentification(PublicKey publicKey, Integer id, Integer unicastPort){
        this.setPublicKey(publicKey);
        this.setIdentification(id);
        this.setUnicastPort(unicastPort);
    }

    /**
     * Gets private key.
     *
     * @return PrivateKey private key
     */
    public PrivateKey getPrivateKey() {
        return this.privateKey;
    }

    /**
     * Gets public key.
     *
     * @return PublicKey public key
     */
    public PublicKey getPublicKey() {
        return this.publicKey;
    }

    /**
     * Sets key pair.
     *
     * @param keys the keys
     */
    public void setKeyPair(KeyPair keys) {
        this.publicKey = keys.getPublic();
        this.privateKey = keys.getPrivate();
    }

    /**
     * Set public key.
     *
     * @param publicKey the public key
     */
    public void setPublicKey(PublicKey publicKey){
        this.publicKey = publicKey;
    }

    /**
     * Gets identification.
     *
     * @return Integer identification
     */
    public Integer getIdentification() {
        return this.identification;
    }

    /**
     * Sets identification.
     *
     * @param id the id
     */
    public void setIdentification(Integer id) {
        this.identification = id;
    }

    /**
     * Gets percentage of reliability.
     *
     * @return the percentage of reliability
     */
    public Integer getPercentageOfReliability() {
        return percentageOfReliability;
    }

    /**
     * Sets percentage of reliability.
     *
     * @param percentageOfReliability the percentage of reliability
     */
    public void setPercentageOfReliability(Integer percentageOfReliability) {
        this.percentageOfReliability = percentageOfReliability;
    }

    public Integer getUnicastPort() {
        return unicastPort;
    }

    public void setUnicastPort(Integer unicastPort) {
        this.unicastPort = unicastPort;
    }
}