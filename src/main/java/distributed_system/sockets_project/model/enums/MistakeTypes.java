package distributed_system.sockets_project.model.enums;

public enum MistakeTypes {
	SEND_FAKE_NEWS, FAILED_TO_CHECK_NEWS
}
