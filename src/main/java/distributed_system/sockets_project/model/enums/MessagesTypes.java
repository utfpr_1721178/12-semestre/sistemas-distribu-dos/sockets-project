package distributed_system.sockets_project.model.enums;

public enum MessagesTypes {
	NEW_PROCESS_DETECTED, WELCOME_MESSAGE, NEWS, BYE_BYE, A_PROCESS_MADE_A_MISTAKE
}