package distributed_system.sockets_project.model;

import distributed_system.sockets_project.model.enums.MessagesTypes;

import java.io.Serializable;

public class Header implements Serializable {

	private static final long serialVersionUID = -103105711821268195L;
	private final Integer unicastPort;
	private final MessagesTypes typeOfMessage;
	private final Integer senderProcessIdentification;

	private byte[] messageBody;
	private byte[] signature;

	public Header() {
		this.unicastPort = null;
		this.typeOfMessage = null;
		this.senderProcessIdentification = null;
	}

	public Header(int unicastPort, Integer senderProcessIdentification) {
		this.unicastPort = unicastPort;

		this.typeOfMessage = null;
		this.senderProcessIdentification = senderProcessIdentification;
	}

	public Header(Header header, MessagesTypes type, byte[] messageBody, byte[] signature) {
		this.unicastPort = header.getUnicastSenderPort();
		this.typeOfMessage = type;
		this.senderProcessIdentification = header.getSenderProcessIdentification();
		this.messageBody = messageBody;
		this.signature = signature;
	}

	public int getUnicastSenderPort() {
		return this.unicastPort;
	}

	public MessagesTypes getMessageType() {
		return this.typeOfMessage;
	}

	public Integer getSenderProcessIdentification() {
		return this.senderProcessIdentification;
	}

	public byte[] getMessageBody() {
		return messageBody;
	}

	public byte[] getSignature() {
		return signature;
	}



}