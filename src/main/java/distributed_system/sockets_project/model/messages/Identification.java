package distributed_system.sockets_project.model.messages;

import distributed_system.sockets_project.model.Header;
import distributed_system.sockets_project.model.enums.MessagesTypes;

import java.io.Serializable;
import java.security.PublicKey;

public class Identification implements Serializable {

	private final PublicKey publicKey;

	public Identification(PublicKey publicKey) {
		this.publicKey = publicKey;
	}

	public PublicKey getPublicKey() {
		return this.publicKey;
	}

}
