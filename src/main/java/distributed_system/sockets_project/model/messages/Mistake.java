package distributed_system.sockets_project.model.messages;

import distributed_system.sockets_project.model.enums.MistakeTypes;

import java.io.Serializable;

public class Mistake implements Serializable {

	private final MistakeTypes whatWasTheMistake;

	private final Integer identificationOfTheProcessThatMadeTheError;

	private final String errorMessage;

	public Mistake(Integer identification, MistakeTypes whatWasTheMistake) {
		this.identificationOfTheProcessThatMadeTheError = identification;
		this.whatWasTheMistake = whatWasTheMistake;

		if (this.whatWasTheMistake == MistakeTypes.SEND_FAKE_NEWS) {
			this.errorMessage = "The process identified as " + this.identificationOfTheProcessThatMadeTheError +
					" sent fake news and will have reduced reliability";
		} else {
			this.errorMessage = "The process identified as " + this.identificationOfTheProcessThatMadeTheError +
					" failed to validate the news and will have reduced reliability";
		}
	}

	public MistakeTypes getWhatWasTheMistake() {
		return whatWasTheMistake;
	}

	public Integer getIdentificationOfTheProcessThatMadeTheError() {
		return identificationOfTheProcessThatMadeTheError;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
}
