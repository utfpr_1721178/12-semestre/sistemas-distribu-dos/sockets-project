package distributed_system.sockets_project.model.messages;

import java.io.Serializable;
import java.security.PublicKey;

public class NewProcess implements Serializable {

	private final PublicKey publicKey;

	public NewProcess(PublicKey publicKey) {

		this.publicKey = publicKey;
	}

	public PublicKey getPublicKey() {
		return this.publicKey;
	}

}