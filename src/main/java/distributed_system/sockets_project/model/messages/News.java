package distributed_system.sockets_project.model.messages;

import java.io.Serializable;

public class News implements Serializable {

	private final boolean news;

	public News(boolean news){
		this.news = news;
	}

	public boolean getNews() {
		return this.news;
	}
}
