package distributed_system.sockets_project.model;

public final class HeaderPrototype {

	private static Header header;

	private HeaderPrototype() {
		throw new IllegalStateException("Utility class");
	}

	public static void factory(int unicastPort, Integer senderProcessIdentification) {
		if (header == null) {
			header = new Header(unicastPort, senderProcessIdentification);
		}
	}

	public static Header getHeader() {
		return header;
	}
}