package distributed_system.sockets_project.model;

import distributed_system.sockets_project.controllers.Process;
import distributed_system.sockets_project.model.enums.MenuTypes;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class State {

	public ProcessIdentification processItSelf;
	public Map<Integer, ProcessIdentification> identificationOfOtherProcesses;
	public ArrayList<String> logQueue;
	public MenuTypes currentOption;

	private final Process process;

	public State(Process process, Integer id, KeyPair keys, Integer unicastPort) {

		this.process = process;

		this.processItSelf = new ProcessIdentification(keys, id, unicastPort);
		this.identificationOfOtherProcesses = new HashMap<>();
		this.logQueue = new ArrayList<>();
		this.currentOption = MenuTypes.MAIN_MENU;

		this.processItSelf.setIdentification(id);
		this.processItSelf.setKeyPair(keys);
	}

	public PrivateKey getPrivateKey() {
		return this.processItSelf.getPrivateKey();
	}

	public PublicKey getPublicKey() {
		return this.processItSelf.getPublicKey();
	}

	public void setPairOfKey(KeyPair keys) {
		this.processItSelf.setKeyPair(keys);
	}

	public Integer getCurrentProcessIdentifier() {
		return this.processItSelf.getIdentification();
	}

	public void setCurrentProcessIdentifier(Integer id) {
		this.processItSelf.setIdentification(id);
	}

	public Integer getPercentageReliabilityOfCurrentProcess() {
		return this.processItSelf.getPercentageOfReliability();
	}

	public void increaseTheReliabilityOfTheCurrentProcess() {
		this.processItSelf.setPercentageOfReliability(
				this.processItSelf.getPercentageOfReliability() + 1
		);
		this.process.terminalOutput.showOutput();
	}

	public void decreaseTheReliabilityOfTheCurrentProcess() {
		this.processItSelf.setPercentageOfReliability(
				this.processItSelf.getPercentageOfReliability() - 1
		);
		this.process.terminalOutput.showOutput();
	}

	public void addIdentificationOfAnotherProcess(ProcessIdentification newProcess) {
		this.identificationOfOtherProcesses.put(newProcess.getIdentification(), newProcess);
		this.addNewLog("New process discovered, identified as " + newProcess.getIdentification());
		if (this.currentOption == MenuTypes.PROCESSES) {
			this.process.terminalOutput.showOutput();
		}
	}

	public void decreaseTheReliabilityOfAnotherProcess(Integer identification) {
		this.identificationOfOtherProcesses.get(identification).setPercentageOfReliability(
				this.identificationOfOtherProcesses.get(identification).getPercentageOfReliability() - 1
		);
		if (this.currentOption == MenuTypes.PROCESSES) {
			this.process.terminalOutput.showOutput();
		}
	}

	public void increaseTheReliabilityOfAnotherProcess(Integer identification) {
		this.identificationOfOtherProcesses.get(identification).setPercentageOfReliability(
				this.identificationOfOtherProcesses.get(identification).getPercentageOfReliability() + 1
		);
		if (this.currentOption == MenuTypes.PROCESSES) {
			this.process.terminalOutput.showOutput();
		}
	}

	public void removeOneProcess(Integer processIdentification) {
		this.identificationOfOtherProcesses.remove((Object) processIdentification);
	}

	public boolean thisProcessExist(Integer processIdentification) {
		return this.identificationOfOtherProcesses.containsKey(processIdentification);
	}

	public ProcessIdentification getProcessByIdentification(Integer identification) {
		return this.identificationOfOtherProcesses.get(identification);
	}

	public synchronized void addNewLog(String log) {
		this.logQueue.add(log);
		if (this.currentOption == MenuTypes.LOG) {
			this.process.terminalOutput.showOutput();
		}
	}

}