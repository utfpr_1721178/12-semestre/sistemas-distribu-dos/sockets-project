package distributed_system.sockets_project.utils;

import java.util.Random;

public class ToolsForVerifyingNewsVeracity {

	private static final Random random = new Random();

	private ToolsForVerifyingNewsVeracity() {
		throw new IllegalStateException("Utility class");
	}

	public static boolean verifyTheTruthOfTheNews(boolean news, Integer senderReliability, Integer currentProcessReliability) {

		if (random.nextInt(100) + 1 <= senderReliability) {
			if (random.nextInt(100) + 1 <= currentProcessReliability) {
				return news;
			} else {
				news = !news;
				return news;
			}
		} else {
			news = !news;
			if (random.nextInt(100) + 1 <= currentProcessReliability) {
				return news;
			} else {
				news = !news;
				return news;
			}
		}
	}
}
