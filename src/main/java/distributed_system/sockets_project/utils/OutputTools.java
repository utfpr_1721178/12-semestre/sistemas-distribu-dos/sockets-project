package distributed_system.sockets_project.utils;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public final class OutputTools {

	private static final short WIDTH_OF_LINE = 100;
	private static final short UNUSABLE_SPACE_ON_LINE = 4;

	private OutputTools() {
		throw new IllegalStateException("Utility class");
	}

	public static void drawLine() {
		for (int index = 0; index < WIDTH_OF_LINE; index++) {
			System.out.print("*");
		}
		System.out.print("\n");
	}

	/**
	 * @param message
	 */
	public static void showMessage(String message) {
		if (message.length() <= WIDTH_OF_LINE - UNUSABLE_SPACE_ON_LINE) {
			System.out.print("* " + message);
			completeEmptySpaces(message.length());
			System.out.println(" *");
			return;
		}

		Queue<String> stringQueue = new LinkedList<>(Arrays.asList(message.split(" ")));

		do {
			System.out.print("* ");

			String textToBeWrittenOnASingleLine = stringQueue.remove();
			while (stringQueue.size() > 0 &&
					textToBeWrittenOnASingleLine.length() + stringQueue.peek().length() <= WIDTH_OF_LINE - UNUSABLE_SPACE_ON_LINE
			) {
				textToBeWrittenOnASingleLine += (" " + stringQueue.remove());
			}

			System.out.print(textToBeWrittenOnASingleLine);
			completeEmptySpaces(textToBeWrittenOnASingleLine.length());

			System.out.println(" *");
		} while (stringQueue.size() > 0);

	}

	public static void showCentralizedMessage(String message) {
		if (message.length() > WIDTH_OF_LINE - UNUSABLE_SPACE_ON_LINE) {
			OutputTools.showMessage(message);
		} else {
			System.out.print("* ");

			int usableSpaceOnLine = WIDTH_OF_LINE - UNUSABLE_SPACE_ON_LINE - message.length();

			while (usableSpaceOnLine > (WIDTH_OF_LINE - UNUSABLE_SPACE_ON_LINE - message.length()) / 2) {
				System.out.print(" ");
				usableSpaceOnLine--;
			}

			System.out.print(message);

			while (usableSpaceOnLine > 0) {
				System.out.print(" ");
				usableSpaceOnLine--;
			}

			System.out.println(" *");
		}
	}

	/**
	 * @param sizeOfMessage
	 */
	private static void completeEmptySpaces(int sizeOfMessage) {
		for (int i = 0; i < WIDTH_OF_LINE - UNUSABLE_SPACE_ON_LINE - sizeOfMessage; i++) {
			System.out.print(" ");
		}
	}

}