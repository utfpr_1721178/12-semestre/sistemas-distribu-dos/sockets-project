package distributed_system.sockets_project.utils;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;

/**
 * MessageSignaturesTools
 */
public final class MessageSignaturesTools {

	private static final int NUMBER_OF_BYTES_OF_THE_KEY_PAIR = 2048;

	private MessageSignaturesTools() {
		throw new IllegalStateException("Utility class");
	}

	public static byte[] sign(byte[] message, PrivateKey privateKey)
			throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
		Signature signatureToolsObject;

		signatureToolsObject = Signature.getInstance("SHA256withDSA", "SUN");
		signatureToolsObject.initSign(privateKey);
		signatureToolsObject.update(message);
		return signatureToolsObject.sign();
	}

	public static boolean validatesTheSignatureOnTheMessage(byte[] message, PublicKey publicKey, byte[] signature)
			throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
		Signature signatureToolsObject;

		signatureToolsObject = Signature.getInstance("SHA256withDSA", "SUN");
		signatureToolsObject.initVerify(publicKey);
		signatureToolsObject.update(message);
		return signatureToolsObject.verify(signature);

	}

	public static KeyPair generateKeyPair() throws NoSuchAlgorithmException, NoSuchProviderException {
		KeyPairGenerator keyGen;

		keyGen = KeyPairGenerator.getInstance("DSA", "SUN");
		keyGen.initialize(NUMBER_OF_BYTES_OF_THE_KEY_PAIR);
		return keyGen.generateKeyPair();

	}
}