package distributed_system.sockets_project.utils;

public final class GlobalInformation {

	public static final Integer INITIAL_PERCENTAGE_OF_PROCESS_RELIABILITY = 70;

	public static final String MULTICAST_ADDRESS = "228.5.6.7";

	public static final int MULTICAST_PORT = 6789;

	public static final String[] MAIN_MENU = {
			"Send true news",
			"Send fake news",
			"See list of known process",
			"See process log",
			"Send message with fake signature",
			"Kill yourself"
	};

	public static final String[] LISTS = {
			"Return to main menu"
	};

	public static final Integer MAXIMUM_NUMBER_OF_VISIBLE_RECORDS = 5;

	private GlobalInformation() {
		throw new IllegalStateException("Utility class");
	}
}