package distributed_system.sockets_project.utils;

import java.io.*;

public class ByteConversionTools {

	private ByteConversionTools() { throw new IllegalStateException("Utility class"); }

	public static Object convertByteArrayToObject(byte[] serializedObject) throws IOException, ClassNotFoundException {
		ByteArrayInputStream input = new ByteArrayInputStream(serializedObject);
		ObjectInputStream objectInput = new ObjectInputStream(input);
		return objectInput.readObject();
	}

	public static byte[] convertObjectToByteArray(Object object) throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		ObjectOutputStream objectOutput = new ObjectOutputStream(output);
		objectOutput.writeObject(object);
		return output.toByteArray();
	}
}
