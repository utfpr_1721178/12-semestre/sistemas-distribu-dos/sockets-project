package distributed_system.sockets_project.view;

import distributed_system.sockets_project.controllers.Process;
import distributed_system.sockets_project.model.ProcessIdentification;
import distributed_system.sockets_project.model.enums.MenuTypes;
import distributed_system.sockets_project.utils.GlobalInformation;
import distributed_system.sockets_project.utils.OutputTools;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.util.Scanner;

public class TerminalOutput {
	private final Scanner scanner;

	private final Process process;

	public TerminalOutput(Process process) {
		this.process = process;

		this.scanner = new Scanner(System.in);

		this.showOutput();
	}

	public void userInput() throws IOException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
		char nextChar;
		do {
			this.showOutput();

			nextChar = this.scanner.next().charAt(0);
			if (MenuTypes.MAIN_MENU == this.process.state.currentOption && nextChar >= '0' &&
					nextChar < '0' + GlobalInformation.MAIN_MENU.length - 1) {
				switch (nextChar) {
					case '0':
						this.process.sendsAMessageContainingANewNews(true);
						break;
					case '1':
						this.process.sendsAMessageContainingANewNews(false);
						break;
					case '2':
						this.process.state.currentOption = MenuTypes.PROCESSES;
						break;
					case '3':
						this.process.state.currentOption = MenuTypes.LOG;
						break;
					case '4':
						this.process.sendMessageWithFakeSignature();
						break;
					default:
						break;
				}
			} else {
				if (nextChar == '0') {
					this.process.state.currentOption = MenuTypes.MAIN_MENU;
				}
			}

		} while (!(MenuTypes.MAIN_MENU == this.process.state.currentOption && nextChar == '0' + GlobalInformation.MAIN_MENU.length - 1));

		this.process.closeTheApplication();
	}

	public void drawnHeader() {
		OutputTools.showCentralizedMessage("Process ID: " + this.process.state.getCurrentProcessIdentifier() + " | "
				+ "Process reliability: " + this.process.state.getPercentageReliabilityOfCurrentProcess() + "%");
		OutputTools.drawLine();
	}

	public void drawnOptions(String[] options, boolean selectable) {
		if (!selectable && this.process.state.currentOption != MenuTypes.MAIN_MENU) {
			OutputTools.showCentralizedMessage(" ");
		}
		for (int index = 0; index < options.length; index++) {

			OutputTools.showMessage((selectable ? index + " -> " : "-> ") + options[index]);
			if (!selectable && this.process.state.currentOption != MenuTypes.MAIN_MENU) {
				OutputTools.showCentralizedMessage(" ");
			}
		}
		OutputTools.drawLine();
	}

	public synchronized void showOutput() {
		System.out.println("\n\n\n\n");
		OutputTools.drawLine();

		if (this.process.state.currentOption == MenuTypes.MAIN_MENU) {

			this.drawnOptions(GlobalInformation.MAIN_MENU, true);
			OutputTools.showCentralizedMessage("Main menu");
			OutputTools.drawLine();
		} else {

			if (this.process.state.currentOption == MenuTypes.LOG) {
				String[] optionsList = new String[this.process.state.logQueue.size()];

				for (int index = 0; index < this.process.state.logQueue.size(); index++) {
					optionsList[index] = this.process.state.logQueue.get(index);
				}

				this.drawnOptions(optionsList, false);
			} else {
				if (this.process.state.identificationOfOtherProcesses.isEmpty()) {

					OutputTools.showCentralizedMessage("No known processes");
					OutputTools.drawLine();

				} else {

					String[] optionsList = new String[this.process.state.identificationOfOtherProcesses.size()];
					int index = 0;

					for (ProcessIdentification processIdentification : this.process.state.identificationOfOtherProcesses.values()) {
						optionsList[index++] = ("process identified as " + processIdentification.getIdentification() +
								" and with " + processIdentification.getPercentageOfReliability() + "% reliability");
					}

					this.drawnOptions(optionsList, false);
				}
			}

			this.drawnOptions(GlobalInformation.LISTS, true);
		}
		this.drawnHeader();
	}
}
